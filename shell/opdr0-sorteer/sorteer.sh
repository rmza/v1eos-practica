#!/usr/bin/env bash

if [ -z "$1" ]; then
	echo "Geen pad opgegeven"
	exit 1
fi

mapnaam="Afbeeldingen"
mkdir "$1"/"$mapnaam"

methode=1

if [ $methode == 1 ]; then
	cd $1
	for file in *.png *.jpg; do
		if [ -f "$file" ]; then
			mv "$file" ./"$mapnaam";
		fi
	done
elif [ $methode == 2 ]; then
	cd $1
	for file in $(find . -maxdepth 1 -type f -name "*.png" -or -name "*.jpg"); do
		mv "$file" ./"$mapnaam";
	done
fi
