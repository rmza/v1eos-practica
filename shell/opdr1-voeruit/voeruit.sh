#!/usr/bin/env bash

if [ -z "$1" ]; then
	echo "Geen bestand opgegeven"
	exit 1
fi

ext="${1##*.}"
#echo $ext

case $ext in
	"py")
	python3 $1
	;;
	"sh")
	bash $1
	;;
	"cc")
	cat $1
	;;
	*)
	echo "Niet ondersteund bestandstype"
	;;
esac
