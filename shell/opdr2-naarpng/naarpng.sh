#!/usr/bin/env bash

if [ -z "$1" ]; then
	echo "Geen pad opgegeven"
	exit 1
fi

for i in $1/*.jpg; do
	if [ -f "$i" ]; then # geen if geeft error als er geen bestand is gevonden
		convert -resize 128x128">" "$i" "${i%.*}.png"
		rm $i
	fi
done
