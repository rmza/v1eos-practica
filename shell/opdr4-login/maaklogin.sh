#!/usr/bin/env bash

if [ -z "$1" ]; then
	echo "Geen bestandsnaam opgegeven"
	exit 1
fi

echo "Gebruikersnaam:"
read naam

if [ -z $naam ]; then
	naam=$(whoami)
fi

echo "Inloggen als: $naam"

echo "Wachtwoord:"
while :; do
	# vraag om wachtwoord
	read -s ww1
	if [ "${#ww1}" -lt 8 ]; then
		echo "Wachtwoord moet minimaal 8 karakters lang zijn"
		continue
	fi
	# herhaal
	echo "Herhaal wachtwoord:"
	while :; do	
		read -s ww2
		# controleer
		if [ "$ww1" != "$ww2" ]; then
		#if ! test $ww1 = $ww2; then
			echo "Wachtwoord komt niet overeen"
			continue
		fi
		break 2
	done
done

# maak hash
md5=$(echo -n "$naam" | md5sum)

# maak file
if [ -f "$1" ]; then
	rm "$1"
fi
touch "$1"
chmod u+x "$1"
nl='\n'
str="${naam}${nl}${md5}"
echo -e $str > "$1"

echo "Gebruikersnaam en MD5-hash opgeslagen"
