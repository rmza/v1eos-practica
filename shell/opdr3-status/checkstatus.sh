#!/usr/bin/env bash

if [ "$#" -ne 3 ]; then
	echo "Dit programma vereist 3 argumenten"
	exit 1
fi

# maak log file
if [ -f "$3" ]; then
	rm "$3"
fi
touch "$3"
chmod u+x "$3"

# loop over ieder bestand
for file in $1/ $1/*.*; do
	#echo $2 on $file
	if "$2" "$file" >/dev/null 2>> $3; then
		echo "passed" >> $3
	fi
done
