#!/usr/bin/env bash

valin() {
	local i=0
	# loop over de argumenten
	for v in "$@"; do
		# sla de te vinden waarde over en controleer
		if [ "$i" != 0 ] && [ "$1" == "$v" ]; then return 0; fi		
		((i=i+1))
	done
	return 1
}

if [ -z $1 ] && [ -z $2 ]; then
	echo "Dit programma vereist 2 argumenten"
	exit 1
fi

# check bestandstype
ext="${2##*.}"

if ! valin "$ext" "cc" "cpp"; then
	echo "Verkeerd input bestand"
	exit 1
fi

# compile en run
if g++ -o "$1" "$2"; then ./"$1"; fi
echo