#include "shell.hh"
#include <errno.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <cstring>
using namespace std;

int main()
{ std::string input;

	// ToDo: Vervang onderstaande regel: Laad prompt uit bestand
	std::string prompt = "";

    int fd = syscall(SYS_open, "config.txt", O_RDONLY, 0755);
    char byte[1];
    while(syscall(SYS_read, fd, byte, 1)) {
        prompt.append(byte);
    }
    syscall(SYS_close, fd);

    while(true)
    { std::cout << prompt;                 // Print het prompt
    std::getline(std::cin, input);         // Lees een regel
    if (input == "new_file") new_file();   // Kies de functie
    else if (input == "ls") list();        // op basis van
    else if (input == "src") src();        // de invoer
    else if (input == "find") find();
    else if (input == "seek") seek();
    else if (input == "exit") return 0;
    else if (input == "quit") return 0;
    else if (input == "error") return 1;

    if (std::cin.eof()) return 0; } }      // EOF is een exit

void new_file() // ToDo: Implementeer volgens specificatie.
{
	string filename;
	cout << "Geef een bestandsnaam: ";
	getline(cin, filename);
	string text;
	cout << "Geef wat tekst: ";
	getline(cin, text);	
	int fd = syscall(SYS_creat, filename.c_str(), 0755);
	for (int i = 0; i < text.size(); i++) {
		char byte[] = { text[i] };
		syscall(SYS_write, fd, byte, 1);
	}
	// de testShell string bevat zelf geen <EOF>
	//char byte[] = { '\n', '<', 'E', 'O', 'F', '>' };
	//syscall(SYS_write, fd, byte, sizeof(byte));
	syscall(SYS_close, fd);
}

void list() // ToDo: Implementeer volgens specificatie.
{
	pid_t pid = syscall(SYS_fork);
	// parent
	if (pid > 0) {
		int status;
		syscall(SYS_waitid, P_PID, pid, &status, WEXITED);
	}
	// child
	else {
		const char* argv[] = { "/bin/ls", "-la", nullptr };
		// geen HOME=/ geeft segmentation fault
		const char* envp[] = { "HOME=/", nullptr };
		syscall(SYS_execve, argv[0], argv, envp);
	}
}

void find() // ToDo: Implementeer volgens specificatie.
{
	// cin gebruiken ipv getline geeft bij return tweemaal prompt bericht	
	//char target[255];
	//cin >> target;

	string target;
	//cout << "Geef een zoekopdracht: " << endl;
	getline(cin, target);
	// argv wil een const char*
	char targetc[target.length() + 1];
	strcpy(targetc, target.c_str());
	
	int fd[2];
	int status;
	
	pid_t pid = syscall(SYS_fork);
	
	// child
	if (pid == 0) {	
		if (syscall(SYS_pipe, fd) == -1) {
			cout << "pipe failed" << endl;
			return;
		}		
		pid_t pid2 = syscall(SYS_fork);
		// child
		if (pid2 == 0) {		
			syscall(SYS_dup2, fd[1], STDOUT_FILENO);
			syscall(SYS_close, fd[0]);
			syscall(SYS_close, fd[1]);
			const char* argv[] = { "/bin/find", ".", nullptr };
			const char* envp[] = { "HOME=/", nullptr };
			syscall(SYS_execve, argv[0], argv, envp);
		}
		else {
			// process blijft in find als we hier wachten
			//syscall(SYS_waitid, P_PID, pid2, &status, WEXITED);
			syscall(SYS_dup2, fd[0], STDIN_FILENO);
			syscall(SYS_close, fd[0]);
			syscall(SYS_close, fd[1]);
			const char* argv[] = { "/bin/grep", targetc, nullptr };
			const char* envp[] = { "HOME=/", nullptr };
			syscall(SYS_execve, argv[0], argv, envp);			
		}
	}
	else {
		syscall(SYS_waitid, P_PID, pid, &status, WEXITED);		
	}
}

void seek() // ToDo: Implementeer volgens specificatie.
{
	int fd_seek = syscall(SYS_creat, "seek", 0755); // testShell wil geen .txt
	int fd_loop = syscall(SYS_creat, "loop", 0755);
	
	char byte[1] = { 'x' };
	syscall(SYS_write, fd_seek, byte, 1);
	syscall(SYS_lseek, fd_seek, 5000000, SEEK_CUR);
	syscall(SYS_write, fd_seek, byte, 1);
	
	// dit is veel slomer
	syscall(SYS_write, fd_loop, byte, 1); // schrijf eerst de x
	byte[0] = '\0';
	for (int i = 0; i < 5000000; i++) {
		syscall(SYS_write, fd_loop, byte, 1);
	}
	byte[0] = 'x';
	syscall(SYS_write, fd_loop, byte, 1);
	
	syscall(SYS_close, fd_seek);
	syscall(SYS_close, fd_loop);
}

void src() // Voorbeeld: Gebruikt SYS_open en SYS_read om de source van de shell (shell.cc) te printen.
{ int fd = syscall(SYS_open, "shell.cc", O_RDONLY, 0755); // Gebruik de SYS_open call om een bestand te openen.
  char byte[1];                                           // 0755 zorgt dat het bestand de juiste rechten krijgt (leesbaar is).
  while(syscall(SYS_read, fd, byte, 1))                   // Blijf SYS_read herhalen tot het bestand geheel gelezen is,
    std::cout << byte; }                                  //   zet de gelezen byte in "byte" zodat deze geschreven kan worden.
