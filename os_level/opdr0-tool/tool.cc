#include <iostream>
#include <string>
using namespace std;

int telwoorden(string line, string woord)
{
    int n = 0;
    for (int i = 0; i < line.size(); i++) {
    	// de eerste letter komt overeen
        if (line[i] == woord[0]) {
            int j = 1;
            int c = 1;
            // controleer de rest van het woord
            while (j < woord.size() && i + j < line.size()) {
                if (line[i + j] == woord[j]) {
                    c++;
                }
                j++;
            }
            // aantal overeenkomende karakters is gelijk aan de woordlengte
            if (c == woord.size()) {
                n++;
            }
        }
    }
    return n;
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        cout << "Geen woord opgegeven" << endl;
        return -1;
    }
    string line;
    while (getline(cin, line))
    {
    	int n = telwoorden(line, argv[1]);
        cout << n << endl;
    }
    return 0;
}
