#include <iostream>
#include <string>
using namespace std;

void rotate(int & n, int min, int max, int step) {
    n += step;
    if (n < min) {
        n = max - (min - n) + 1;
    }
    else if (n > max) {
        n = min + (n - max) - 1;
    }
}

string translate(string line, string argument)
{
	// verplaats karakters n positites
    int step = stoi(argument);
	string result = "";
    int len = line.size();
	for (int i = 0; i < len; i++) {
        // roteer decimaal van char
        char ch = line[i];
        int n = ch;
        rotate(n, 32, 126, step);
        ch = n;
        result.push_back(ch);
    }
  	return result;
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        cout << "Deze functie heeft exact 1 argument nodig" << endl;
        return -1;
    }
    string line;
    while (getline(cin, line))
    {
        cout << translate(line, argv[1]) << endl;
    }
    return 0;
}
